//Capa de red de Usuario

const express = require('express');

//Archivo general de respuestas
const response = require('../../../network/response');

//Controlador
const controller = require('./index');

//El route manejara todas las rutas de usuarios
const router = express.Router();

//Rutas
router.get('/', list);
router.get('/:id', get);
router.post('/', upsert);
router.put('/', upsert);

//Internal Functions
function list(req,res){
    controller.list()
        .then((lista) => {
            response.success(req,res,lista,200);
        })
        .catch((e) => {
            response.error(req,res,e.message,500);
        });
}

function get(req,res){
    controller.get(req.params.id)
        .then((user) => {
            response.success(req,res,user,200);
        })
        .catch((e) => {
            response.error(req,res,e.message,500);
        });
}

function upsert(req,res){
    controller.upsert(req.body)
        .then((user) => {
            response.success(req, res, user, 201);
        })
        .catch((e) => {
            response.error(req, res, e.message, 500);
        })
}

module.exports = router;