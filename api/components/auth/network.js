//Capa de red de Auth

const express = require('express');

//Archivo general de respuestas
const response = require('../../../network/response');

//Controlador
const controller = require('./index');

//El route manejara todas las rutas de Auth
const router = express.Router();

router.post('/login', function(req,res) {
    controller.login(req.body.username, req.body.password)
        .then((token) => {
            response.success(req,res,token,200);
        })
        .catch((err) => {
            response.error(req,res,'Informacion invalida', 400);
        })
});

module.exports = router;