//Servidor Express
const express = require('express');

//Importo libreria swagger que me permite documentar mi api
const swaggerUI = require('swagger-ui-express');

//Configuraciones generales
const config = require('../config.js');

//Capa de red de Usuario
const user = require('./components/user/network');
//Capa de red de autorizacion
const auth = require('./components/auth/network');

//Creamos el servidor
const app = express();

//Usamos el bodyParser que viene en express por defecto
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//Obtengo el json con mi documentacion
const swaggerDoc = require('./swagger.json');

//RUTAS
app.use('/api/user', user);
app.use('/api/auth', auth);
app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerDoc));

//Le decimos al servidor que escuche en un puerto
app.listen(config.api.port, () => {
    console.log('Api escuchando en el puerto ', config.api.port);
});